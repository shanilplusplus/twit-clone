import React from 'react'
import ReactDOM from 'react-dom'
import data from './data.json'
import Favorites from './liked.js'
import './css/main.scss'
import FaHome from 'react-icons/lib/fa/home';
import FaHeart from 'react-icons/lib/fa/heart';
import{Router, Route, browserHistory, Link} from 'react-router'; 


// Router Component
class App extends React.Component{ 
	render() {
		return(
			<Router history={browserHistory}>
			<Route path={'/'} component={Home} ></Route>
			<Route path={'/favorites'} component={Favorites} ></Route>
			</Router>
		);
	}
}

// Home Component to view all posts
class Home extends React.Component{
    constructor(){
        super();
        this.state = {
            data
        }
    }
    //toggle state when liked
    handleToggle(index){
    var currentState = this.state.data;
    //get the index of the posts clicked
    currentState[index].liked = !currentState[index].liked
    //stateless dummy elementused to increment likes counter
    if(currentState[index].liked){
        currentState[index].likes++
    }else{
        currentState[index].likes--
    }
     this.setState({
       currentState : currentState
    })
    }
    render(){
        var data = this.state.data;
        //map out the state data
        data = data.map(function(item, index){
            return(
                <div className="profile">
                 <div className="author-info">
                 <img src={item.avatar} className="author-avatar" />
                 <p>{item.name}</p>
                 </div>
                 <div className="body">
                    <img src={item.picture} className="main-image" />
                    <div className="on-image">
                        <p className="item-title"> {item.title} </p>
                        <span className={item.liked? 'heart-liked' : 'heart-unliked'} onClick={this.handleToggle.bind(this, index)}> <FaHeart /></span>
                    </div>
                 </div>
                 <div className="details"> 
                    <p className="like-count"> <FaHeart />{item.likes} likes</p>
                    <p>{item.description}</p>
                    <p className="item-tags">{item.tags}</p>
                    <p className="comments">{item.comments}</p>
                 </div>
                </div>
            )
        }.bind(this));
        return(
            <div className="container">
            <nav className="nav">
            <ul> 
                <li> <Link to={'/'}><FaHome /><span className="nav-item">Home </span></Link></li>
                <li> <Link to={'/favorites'}><FaHeart /><span className="nav-item">Favorites </span></Link></li>
            </ul>
            </nav>
            <div className="body-wrapper">{data}</div>
            </div>
        )
    }
}


ReactDOM.render(<App />, document.getElementById('app'));