import React from 'react'
import ReactDOM from 'react-dom'
import data from './data.json'
import FaHome from 'react-icons/lib/fa/home';
import FaHeart from 'react-icons/lib/fa/heart';
import{Router, Route, browserHistory, Link} from 'react-router'; 

//Favorites component to view liked posts
class Favorites extends React.Component{
    constructor(){
        super();
        this.state = {
            data
        }
    }
    handleToggle(index){
        var currentState = this.state.data;
        currentState[index].liked = !currentState[index].liked
        if(currentState[index].liked){
            currentState[index].likes++
        }else{
            currentState[index].likes--
        }
         this.setState({
           currentState : currentState
        })
    }
    render(){
        var data = this.state.data;
     data = data.map(function(item, index){
         if(item.liked){
            return(
                <div className="profile">
                 <div className="author-info">
                 <img src={item.avatar} className="author-avatar" />
                 <p>{item.name}</p>
                 </div>
                 <div className="body">
                    <img src={item.picture} className="main-image" />
                    <div className="on-image">
                        <p className="item-title"> {item.title} </p>
                        <span className={item.liked? 'heart-liked' : 'heart-unliked'} onClick={this.handleToggle.bind(this, index)}> <FaHeart /></span>
                    </div>
                 </div>
                 <div className="details">
                    <p className="like-count"><FaHeart />{item.likes} likes</p>
                    <p>{item.description}</p>
                    <p className="item-tags">{item.tags}</p>
                    <p className="comments">{item.comments}</p>
                 </div>
                </div>
            )
         } else{
             return null
         }
        
    }.bind(this));
        return(
            <div>
            <nav className="nav">
            <ul> 
                <li> <Link to={'/'}><FaHome /><span className="nav-item">Home </span></Link></li>
                <li> <Link to={'/favorites'}><FaHeart /><span className="nav-item">Favorites </span></Link></li>
            </ul>
            </nav>
            <div className="body-wrapper">{data}</div>
        </div>
        );
    }
}
module.exports =  Favorites;