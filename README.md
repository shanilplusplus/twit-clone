# Twitr

SPA using React.js to like posts and view the liked posts

### Prerequisites

you need to have node installed in your computer. Moreover install webpack gloablly on your machine.

```
npm install webpack -g
```

## Getting Started

to get started on you local machine, clone this repo, and install all the node modules. Then bundle a production ready copy using webpack. Finally, fire up the node server.


### Installing
cd into the folder
install npm dependencies


### Running Locally

```
npm install
webpack -p
node server.js
```

### View Demo
[twitr](https://shanil-twitr.herokuapp.com/)

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [webpack](https://webpack.github.io/) - bundling
* [babel](https://rometools.github.io/rome/) - compiling and transpling
* [React router](https://rometools.github.io/rome/) - Routing
* [sass](https://rometools.github.io/rome/) - for scss


## Authors

* **Shanil Sigera** (http://shanilplusplus.com/)


## License

This project is licensed under the MIT License


